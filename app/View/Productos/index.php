<?php
require  RUTA_APP . "/View/inc/header.php";
require RUTA_APP . "/View/inc/sidebar.php";
?>

<div class="app-main__outer">

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-box1 icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Productos en stock
                        <div class="page-title-subheading">Informacion de disponibilidad de productos
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="card col-12">
                <div class="card-header">
                    <a href="<?php echo RUTA_URL; ?>Productos/Create" class="btn btn-success">Registro de nuevo producto</a>
                </div>
                <div class="card-body">

                    

                        <table id="table_id" class="table">
                            <thead>
                                <tr>
                                    <th>

                                    </th>
                                    <th scope="col">
                                        Nombre Producto
                                    </th>
                                    <th scope="col">
                                        Stock
                                    </th>
                                    <th scope="col">
                                        Precio
                                    </th>
                                    <th scope="col">
                                        Descripcion
                                    </th>
                                    
                                    <th>

                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                <?php

                                foreach ($data["productos"] as $productos => $producto) {
                                ?>
                                    <tr>
                                        <td><a href="<?php echo BASE  ?>Productos/show/<?php echo $producto->id?> "><img class='Picimg' style='height: 8rem;width:6em;' src="<?php echo BASE ?>images/Suplementos/<?= $producto->ImageProduct ?>"></a></td>
                                        <td><?= $producto->nombre_p ?></td>
                                        <td><?= $producto->Stock ?></td>
                                        <td>$<?= $producto->Precio ?></td>
                                        <td><?= $producto->Descripcion ?></td>
                                        
                                        </tr>
                                <?php

                                }
                                
                                ?>

                        
                            </tbody>
                        </table>


                    
                </div>
            </div>
        </div>





    </div>


</div>

</div>
<!-- FIN DE app-container en Header-->




<?php

require RUTA_APP . "/View/inc/footer.php";
?>

<script>
     $(document).ready(function() {
         $('#table_id').DataTable({
             pageLength: 10,
             columnDefs: [{
                 orderable: false,
                 className: 'select-checkbox',
                 targets: 0
             }],
             select: {
                 style: 'multi',
                 selector: 'td:first-child'
             }
         });
     });

     
     
  

     document.querySelector(".table").addEventListener('mouseover',(e) => {
      
            if(e.target.className == "Picimg"){
                e.target.style.transform = "scale(1.5)";
            }

            //console.log(e.target.className       
     });

     document.querySelector(".table").addEventListener('mouseout',(e) => {
      
      if(e.target.className == "Picimg"){
          e.target.style.transform = "scale(1.0)";
          //console.log("leave");
      }

      //console.log(e.target.className       
});


  
 </script>
