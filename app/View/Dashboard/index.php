<?php
require  RUTA_APP . "/View/inc/header.php";
require RUTA_APP . "/View/inc/sidebar.php";
?>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-graph2 icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>dashboard del cliente
                       
                    </div>
                </div>
                
            </div>
        </div>

   <!-- mostrar maquina -->
   <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Estado de maquinas de entrenamiento
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <button class="active btn btn-focus" id="actualizar">Actualizar</button>
                             
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table" id="table_id">
                            <thead>
                                <tr>
                                    
                                    <th scope="col">Nombre Maquina</th>
                                    <th scope="col">Estado</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
    <div>
</div>
<?php require RUTA_APP . "/View/inc/footer.php";?>

<script>

    $(function(){
        tabla();
    });
    function tabla(){
        $.ajax({
            url:"MainMenu/showData",
            type:"POST",
            success: function(res){
                var js = JSON.parse(res);
                var tabla;

                for (let index = 0; index < js.length; index++) {
                    if(js[index].Estado == "Inactivo"){
                        tabla+="<tr><td>"+js[index].NombreMaquina+"</td><td><div class='badge badge-danger'>"+js[index].Estado+"</div></td></tr>";
                    }else{
                        tabla+="<tr><td>"+js[index].NombreMaquina+"</td><td><div class='badge badge-success'>"+js[index].Estado+"</div></td></tr>";
                    }
                    
                    
                }
                $("#tbody").html(tabla);
            }
        });
    }
    $(document).ready(function(){
        $('#table_id').DataTable({
            pageLength: 10,
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            }
        });

    });
    $("#actualizar").click(function(){
        tabla();
    });

        </script>